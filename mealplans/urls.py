from django.urls import path

from mealplans.views import (
    MealPlanListView,
    MealPlanDetails,
    MealPlanCreateView,
    MealPlanEditView,
    MealPlanDeleteView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="mealplan_list"),
    path("<int:pk>/", MealPlanDetails.as_view(), name="mealplan_details"),
    path(
        "<int:pk>/delete/", MealPlanDeleteView.as_view(), name="mealplan_delete"
    ),
    path("new/", MealPlanCreateView.as_view(), name="mealplan_new"),
    path("<int:pk>/edit/", MealPlanEditView.as_view(), name="mealplan_edit"),
]
