from django.urls import reverse_lazy
from .models import MealPlan
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)


class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "mealplans/list.html"
    paginate_by = 8

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDetails(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "mealplans/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "mealplans/new.html"
    fields = ["name", "description", "startdate", "recipes"]
    success_url = reverse_lazy("mealplan_list")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class MealPlanEditView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "mealplans/edit.html"
    fields = ["name", "description", "startdate", "recipes"]

    def get_success_url(self):
        mealplanid = self.kwargs["pk"]
        return reverse_lazy("mealplan_details", kwargs={"pk": mealplanid})


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "mealplans/delete.html"
    success_url = reverse_lazy("mealplan_list")
