from django import template

register = template.Library()


@register.simple_tag
def resize_to(ingredient, target):
    def_servings = ingredient.recipe.servings

    if def_servings is not None and target is not None:
        try:
            ratio = int(target) / def_servings
            return ratio * ingredient.amount
        except ValueError:
            pass
        except TypeError:
            print("convert type")
            pass
    return ingredient.amount


register.filter(resize_to)
