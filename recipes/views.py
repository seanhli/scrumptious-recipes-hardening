from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.db import IntegrityError

from recipes.forms import RatingForm
from recipes.models import Recipe, ShoppingItem, Ingredient

from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.http import require_http_methods


def log_rating(request, recipe_id):
    try:
        if request.method == "POST":
            form = RatingForm(request.POST)
            if form.is_valid():
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
    except Recipe.DoesNotExist:
        return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 6
    sort_method = "-updated"

    def post(self, request):
        self.sort_method = request.POST.get("sort_type")
        self.object_list = self.get_queryset()
        context = self.get_context_data()
        return self.render_to_response(context)

    def get_queryset(self):
        return Recipe.objects.order_by(self.sort_method)


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"
    current_serving_size = Recipe.servings

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        shopping_items = [
            item.food_item for item in self.request.user.ShoppingItem.all()
        ]

        context["shopping_list_foods"] = shopping_items
        context["current_serving_size"] = Recipe.servings

        return context

    def post(self, request, **kwargs):
        self.current_serving_size = request.POST.get("serving_scaling")
        self.object = self.get_object()
        context = self.get_context_data(**kwargs)
        context["current_serving_size"] = self.current_serving_size
        return self.render_to_response(context)


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image", "servings"]

    def get_success_url(self):
        recipeid = self.kwargs["pk"]
        return reverse_lazy("recipe_detail", kwargs={"pk": recipeid})

    def get_queryset(self):
        return Recipe.objects.filter(author=self.request.user)


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")

    def get_queryset(self):
        return Recipe.objects.filter(author=self.request.user)


class ShoppingListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shoppingitems/list.html"
    success_url = reverse_lazy("shopping_list")

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


@require_http_methods(["POST"])
def ShoppingItemCreate(request):
    ingredient_id = request.POST.get("ingredient_id")
    ingredient = Ingredient.objects.get(id=ingredient_id)
    current_user = request.user

    try:
        ShoppingItem.objects.create(
            user=current_user, food_item=ingredient.food
        )
    except IntegrityError:
        pass

    return redirect("recipe_detail", pk=ingredient.recipe.id)


@require_http_methods(["POST"])
def ShoppingListDelete(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_list")
